import { useRouter } from "next/router";
import { auth, signInWithEmailAndPassword } from "../../services/firebase-auth";
import store from "../../redux";
import actionTypes from "../../redux/action";

const LoginButton = ({ email, password }) => {
  const router = useRouter();
  const handleClick = (e) => {
    e.preventDefault();

    signInWithEmailAndPassword(auth, email, password)
      .then((userCredential) => {
        const userAuth = userCredential.user;

        store.dispatch({
          type: actionTypes.SET_USER,
          user: userAuth,
        });

        store.dispatch({ type: "SHOW_LOADER" });
        router.push("/dashboard");
      })
      .catch((error) => {
        const errorCode = error.code;
        const errorMessage = error.message;
        alert(errorCode, errorMessage);
      });
  };

  return (
    <button
      type="button"
      className="w-100 login__btn"
      onClick={(e) => handleClick(e)}
    >
      Login
    </button>
  );
};

export default LoginButton;
