import { useRouter } from "next/router";
import {
  auth,
  googleProvider,
  signInWithPopup,
} from "../../services/firebase-auth";
import db from "../../services/firebase-storage";
import store from "../../redux";
import actionTypes from "../../redux/action";

const LoginGoogleButton = ({ text }) => {
  const router = useRouter();
  const userCollection = db.collection("users");

  const handleClick = (e) => {
    e.preventDefault();

    signInWithPopup(auth, googleProvider)
      .then((userCredential) => {
        const userAuth = userCredential.user;
        const userDB = userCollection.doc(userAuth.uid);

        store.dispatch({
          type: actionTypes.SET_USER,
          user: userAuth,
        });

        userDB.get().then((doc) => {
          if (!doc.exists) {
            userCollection.doc(userAuth.uid).set({
              uid: userAuth.uid,
              name: userAuth.displayName,
              authProvider: "google",
              email: userAuth.email,
              win: 0,
              lose: 0,
              gameHistory: null,
              achievement: null,
              score: 0,
            });
          }
        });
        router.push("/dashboard");
      })
      .catch((error) => console.error(error));
  };

  return (
    <button
      type="button"
      className="login__btn login__google w-100"
      onClick={(e) => handleClick(e)}
    >
      {text}
    </button>
  );
};

export default LoginGoogleButton;
