import React from "react";
import { Button } from "react-bootstrap";
import styles from "../../styles/Home.module.css";

function Hero() {
  return (
    <section className={styles.section_hero}>
      <div className="container">
        <div className="row">
          <div className="col col-md-6 mt-5">
            <p className={styles.title_hero}>PLAY TOGETHER WITH YOURS FRIEND</p>
            <Button className={styles.button_hero} href="/games/batu-kertas-gunting" size="lg">
              GET STARTED
            </Button>
          </div>
        </div>
      </div>
    </section>
  );
}

export default Hero;
