import Features from "./Features";
import Hero from "./Hero";
import Navigation from "./Navbar";
import Testimonial from "./Testimonial";
import Footer from "./Footer";

export { Features, Hero, Navigation, Testimonial, Footer };
