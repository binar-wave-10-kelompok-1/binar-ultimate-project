import Link from "next/link";
import { useEffect, useState } from "react";
import { Navbar, Nav, Container } from "react-bootstrap";
import { connect } from "react-redux";
import styles from "../../styles/Home.module.css";

function Navigation({ user, isCentered }) {
  const [userId, setUserId] = useState("");

  useEffect(() => {
    const dataUser = () => {
      if (user.user) {
        setUserId(user.user.uid);
      } else {
        setUserId(user.uid);
      }
    };
    dataUser();
  }, []);

  if (isCentered) {
    return (
      <>
        <Navbar className={styles.nav_wrapper}>
          <Container className="justify-content-center">
            <Navbar.Brand href="/" className="fs-4 text-white">
              LOGO
            </Navbar.Brand>
          </Container>
        </Navbar>
      </>
    );
  }

  return (
    <>
      <Navbar className={["navbar-collapse", styles.nav_wrapper]} expand="lg">
        <Container>
          <Navbar.Brand href="/" className="fs-4 text-white">
            LOGO
          </Navbar.Brand>
          <Navbar.Toggle className="border-white" />
          <Navbar.Collapse>
            <Nav className="d-flex ms-auto justfiy-content-right">
              <Nav.Link className={[styles.nav_item, "text-white"]} href="/">
                Home
              </Nav.Link>
              <Nav.Link className={[styles.nav_item, "text-white"]} href="/games/game-list">
                List Game
              </Nav.Link>

              {userId ? (
                <Nav>
                  <Nav.Link className={[styles.nav_item, "text-white"]} href="/dashboard">
                    Profile
                  </Nav.Link>
                  <Nav.Link className={[styles.nav_item, "text-white"]} href="/score">
                    Score
                  </Nav.Link>
                </Nav>
              ) : (
                <Nav className="me-5 mt-2 d-flex justfiy-content-right flex-row">
                  <Link href="/login">
                    <a className={`${styles.nav_item} text-white me-2`}>Login</a>
                  </Link>
                  <Link href="/register">
                    <a className={`${styles.nav_item} text-white`}>Register</a>
                  </Link>
                </Nav>
              )}
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </>
  );
}

const mapStateToProps = (state) => ({
  user: state.user,
});

export default connect(mapStateToProps)(Navigation);
