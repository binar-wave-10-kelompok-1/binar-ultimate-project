import { Nav, Navbar, Container } from "react-bootstrap";
import styles from "../../styles/Home.module.css";

function Footer() {
  return (
    <footer className={styles.footer}>
      <Navbar>
        <Container className="align-items-start flex-column flex-md-row">
          <Navbar.Brand href="/" className="fs-4 text-white">
            LOGO
          </Navbar.Brand>
          <Nav className="flex-column flex-md-row">
            <Nav.Item className="mx-md-4">
              <h4 className="px-2 fw-bold">Home</h4>
              <Nav.Link href="#features" className="text-white pt-0">
                Features
              </Nav.Link>
              <Nav.Link href="#testimonial" className="text-white pt-0">
                Testimonial
              </Nav.Link>
            </Nav.Item>
            <Nav.Item className="mx-md-4">
              <h4 className="px-2 fw-bold">Pages</h4>
              <Nav.Link href="#features" className="text-white pt-0">
                About me
              </Nav.Link>
              <Nav.Link href="#testimonial" className="text-white pt-0">
                List Game
              </Nav.Link>
              <Nav.Link href="#testimonial" className="text-white pt-0">
                Carrers
              </Nav.Link>
              <Nav.Link href="#testimonial" className="text-white pt-0">
                Pricing
              </Nav.Link>
            </Nav.Item>
            <Nav.Item className="mx-md-4">
              <h4 className="px-2 fw-bold">Blog</h4>
              <Nav.Link href="#features" className="text-white pt-0">
                Blog Listing
              </Nav.Link>
              <Nav.Link href="#testimonial" className="text-white pt-0">
                Blog Article
              </Nav.Link>
              <Nav.Link href="#testimonial" className="text-white pt-0">
                Newsroom
              </Nav.Link>
            </Nav.Item>
          </Nav>
        </Container>
      </Navbar>
      <div className="container">
        <div className="d-flex">
          <a href="#" className="fs-3 mx-2 text-white pe-auto">
            <i>
              <ion-icon name="logo-facebook" />
            </i>
          </a>
          <a href="#" className="fs-3 mx-2 text-white pe-auto">
            <i>
              <ion-icon name="logo-twitter" />
            </i>
          </a>
          <a href="#" className="fs-3 mx-2 text-white pe-auto">
            <i>
              <ion-icon name="logo-youtube" />
            </i>
          </a>
        </div>
        <div className="text-center">
          <p>© 2021 Binar Academy FSW-10 Team 1</p>
        </div>
      </div>
    </footer>
  );
}

export default Footer;
