import Image from "next/image";
import styles from "../../styles/Home.module.css";

function Features() {
  return (
    <section className={styles.section_features}>
      <div className="container h-100">
        <div className="row h-100 flex-column flex-lg-row justify-content-center">
          <div className="col col-md-5">
            <h5>FEATURES</h5>
            <p className={styles.title_feature}>What we are offering</p>
            <small>
              We offer several features, our superior features are game reports
              and game launches. There will be some features in the future
            </small>
          </div>
          <div className="col col-md-7 mt-5 mt-lg-0">
            <div className={styles.list_features}>
              <div className="p-2">
                <Image src="/images/feature-web.png" width="50" height="50" />
                <h6>Website Responsive</h6>
                <p>
                  A responsive website that can be accessed on various screen
                  sizes from gadgets to desktops
                </p>
              </div>
              <div className="p-2">
                <Image
                  src="/images/feature-game-launch.png"
                  width="50"
                  height="50"
                />
                <h6>Game Launch</h6>
                <p>
                  Games that can be played directly on the spot without the need
                  to download them
                </p>
              </div>
              <div className="p-2">
                <Image
                  src="/images/feature-social-media.png"
                  width="50"
                  height="50"
                />
                <h6>Social Media</h6>
                <p>Games that can be shared on various social media</p>
              </div>
              <div className="p-2">
                <Image
                  src="/images/feature-report.png"
                  width="50"
                  height="50"
                />
                <h6>Game Reports</h6>
                <p>
                  Game reports that let players see their scores and results
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

export default Features;
