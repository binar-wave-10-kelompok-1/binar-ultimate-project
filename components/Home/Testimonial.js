import React from "react";
import SwiperCore, { Pagination, Navigation } from "swiper";
import { Swiper, SwiperSlide } from "swiper/react";
import { Card } from "react-bootstrap";
import styles from "../../styles/Home.module.css";
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";

SwiperCore.use([Pagination, Navigation]);

function Testimonial() {
  return (
    <section className={styles.section_testimonial}>
      <div className="container">
        <h5>TESTIMONIAL</h5>
        <p className={styles.title_testimonial}>What are they saying</p>
        <div className="d-flex">
          <Swiper slidesPerView={3} spaceBetween={30} freeMode navigation pagination={{ clickable: true }} className="mySwiper pt-5">
            <SwiperSlide>
              <Card border="white" className={styles.card_wrapper}>
                <Card.Body>
                  <div className="d-flex">
                    <Card.Img src="/images/user-1.svg" className={styles.img_user} alt="avatar-user" />
                    <div className="ms-3">
                      <Card.Title>John Doe</Card.Title>
                      <Card.Subtitle className="mb-2">CEO of ASD.ai</Card.Subtitle>
                    </div>
                  </div>
                  <Card.Text>
                    I really enjoy the games on this website, I really like the rps games that are offered and some other games, great fun
                  </Card.Text>
                </Card.Body>
              </Card>
            </SwiperSlide>
            <SwiperSlide>
              <Card border="white" className={styles.card_wrapper}>
                <Card.Body>
                  <div className="d-flex">
                    <Card.Img src="/images/user-1.svg" className={styles.img_user} alt="avatar-user" />
                    <div className="ms-3">
                      <Card.Title>John Foe</Card.Title>
                      <Card.Subtitle className="mb-2">CEO of XYZ.ai</Card.Subtitle>
                    </div>
                  </div>
                  <Card.Text>
                    I really like playing games here, there are many games and features in it, and the feature I like the most is the game report
                  </Card.Text>
                </Card.Body>
              </Card>
            </SwiperSlide>
            <SwiperSlide>
              <Card border="white" className={styles.card_wrapper}>
                <Card.Body>
                  <div className="d-flex">
                    <Card.Img src="/images/user-1.svg" className={styles.img_user} alt="avatar-user" />
                    <div className="ms-3">
                      <Card.Title>John Doe</Card.Title>
                      <Card.Subtitle className="mb-2">CEO of ASD.ai</Card.Subtitle>
                    </div>
                  </div>
                  <Card.Text>
                    I really enjoy the games on this website, I really like the rps games that are offered and some other games, great fun
                  </Card.Text>
                </Card.Body>
              </Card>
            </SwiperSlide>
            <SwiperSlide>
              <Card border="white" className={styles.card_wrapper}>
                <Card.Body>
                  <div className="d-flex">
                    <Card.Img src="/images/user-1.svg" className={styles.img_user} alt="avatar-user" />
                    <div className="ms-3">
                      <Card.Title>John Foe</Card.Title>
                      <Card.Subtitle className="mb-2">CEO of XYZ.ai</Card.Subtitle>
                    </div>
                  </div>
                  <Card.Text>
                    I really like playing games here, there are many games and features in it, and the feature I like the most is the game report
                  </Card.Text>
                </Card.Body>
              </Card>
            </SwiperSlide>
          </Swiper>
        </div>
      </div>
    </section>
  );
}

export default Testimonial;
