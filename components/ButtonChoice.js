import { useEffect, useRef, useState } from "react";

const ButtonChoice = ({ user, choice }) => {
  const [picks, setPicks] = useState(null);
  const ref = useRef(choice);

  useEffect(() => setPicks(choice), [choice]);

  return (
    <button
      type="button"
      className={`choice ${choice}`}
      ref={ref}
      data-choice={choice}
      data-user={user}
    >
      <img
        loading="lazy"
        width="128px"
        height="128px"
        src={`/images/${picks}.png`}
        alt={picks}
      />
    </button>
  );
};

export default ButtonChoice;
