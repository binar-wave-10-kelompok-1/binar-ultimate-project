import React, { Component } from "react";
import { connect } from "react-redux";
import Image from "next/image";

class FullPageLoader extends Component {
  // eslint-disable-next-line react/state-in-constructor
  state = {};

  render() {
    const { loading } = this.props;

    if (!loading) return null;

    return (
      <div className="loader-container">
        <div className="loader">
          <Image src="/images/loader.gif" width={200} height={200} />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({ loading: state.loading.loading });

export default connect(mapStateToProps)(FullPageLoader);
