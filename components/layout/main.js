import Header from "../Header";

const LayoutMain = ({ children }) => (
  <>
    <Header />
    {children}
  </>
);

export default LayoutMain;
