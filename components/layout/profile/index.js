import Navigation from "../../Home/Navbar";
import LayoutMain from "../main";
import ProfileMain from "./ProfileMain";

function ProfileLayout({ children }) {
  return (
    <LayoutMain>
      <Navigation isCentered />
      <ProfileMain>{children}</ProfileMain>
    </LayoutMain>
  );
}

export default ProfileLayout;
