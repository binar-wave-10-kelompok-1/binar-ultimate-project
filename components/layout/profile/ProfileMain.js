import { Nav, Navbar } from "react-bootstrap";
import Link from "next/link";
import styles from "../../../styles/Profile.module.css";

function ProfileMain({ children }) {
  return (
    <div className="container">
      <div className="mt-5 mb-2 ms-1">
        <i>
          <ion-icon name="person-outline" />
        </i>
        <span className={styles.profile_name}>Nama User</span>
      </div>
      <div className={styles.content_wrap}>
        <Navbar className="w-100 p-0">
          <Nav className="w-100">
            <Nav.Item className={styles.content_nav}>
              <Link href="/profile">
                <a className={styles.content_nav_item}>Profile</a>
              </Link>
              <Link href="/users">
                <a className={styles.content_nav_item}>All Users</a>
              </Link>
            </Nav.Item>
          </Nav>
        </Navbar>
        {children}
      </div>
    </div>
  );
}

export default ProfileMain;
