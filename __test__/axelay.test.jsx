/* eslint-disable no-undef */
import React from 'react'
import { render, screen } from '@testing-library/react'
import Axelay from '../pages/games/details/axelay'
import '@testing-library/jest-dom';


describe('Check misspelling', () => {
  it('Renders a Text in description', () => {
    render(<Axelay />)

    const heading = screen.getByText(/Took/i)
    expect(heading).toBeInTheDocument()
  })
})