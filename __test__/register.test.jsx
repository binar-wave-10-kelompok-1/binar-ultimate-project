import React from 'react'
import { render, screen } from '@testing-library/react'
import Register from '../pages/register'

describe('Register placeholder', () => {
  it('register', () => {
    const queryByPlaceholderName = render(<Register/>)
    expect(queryByPlaceholderName("Full Name")).toBeTruthy()
  })
})

describe('Register Text', () => {
  it('register', () => {
    render(<Register/>)
    const rend = screen.getByText(/Register/i)
    expect(rend).toBeInTheDocument()
  })
})