/* eslint-disable no-undef */
import React from 'react'
import { render, screen } from '@testing-library/react'
import Card from '../pages/games/game-list'
import '@testing-library/jest-dom';


describe('Check Classname= Card', () => {
  it('renders Card class', () => {
    render(<Card />)

    const heading = screen.getByTestId("cardCheck")
    expect(heading).toHaveClass('card')
  })
})