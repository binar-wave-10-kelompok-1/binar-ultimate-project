/* eslint-disable no-undef */
import React from 'react'
import { render, screen } from '@testing-library/react'
import Song from '../pages/games/game-list'
import '@testing-library/jest-dom';


describe('Find Song title', () => {
  it('Renders a Song title', () => {
    render(<Song />)

    const heading = screen.getByText(/One Winged Angel/i)
    expect(heading).toBeInTheDocument()
  })
})