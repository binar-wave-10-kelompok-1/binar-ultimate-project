import React from 'react'
import { render, screen } from '@testing-library/react'
import Login from '../pages/login'

describe('Login placeholder', () => {
  it('login', () => {
    const queryByPlaceholderName = render(<Login/>)
    expect(queryByPlaceholderName("Password")).toBeTruthy()
  })
})

describe('Login found text', () => {
  it('login', () => {
    render(<Login/>)
    const rend = screen.getByText(/Register/i)
    expect(rend).toBeInTheDocument()
  })
})