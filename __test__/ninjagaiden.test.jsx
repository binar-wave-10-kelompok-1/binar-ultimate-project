/* eslint-disable no-undef */
import React from 'react'
import { render, screen } from '@testing-library/react'
import Home from '../pages/games/details/ninja-gaiden-trilogy'
import '@testing-library/jest-dom';


describe('Home', () => {
it('renders Button Enable', () => {
    render(<Home />)

    const heading = screen.getByTestId("ninjabutton")
    expect(heading).toBeEnabled()
})
})