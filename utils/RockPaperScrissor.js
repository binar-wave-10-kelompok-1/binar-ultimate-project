/* eslint-disable max-classes-per-file */
import { collection } from "../db/user";
import { firestoreArrayUnion, firestoreIncrement } from "../services/firebase";
import actionTypes from "../redux/action";

function PlayGames(user, games, dispatch) {
  class Player {
    constructor() {
      this.batu = document.getElementsByClassName("batu");
      this.kertas = document.getElementsByClassName("kertas");
      this.gunting = document.getElementsByClassName("gunting");
      this.choice = null;
    }
  }

  const Computer = (Base) =>
    class extends Base {
      randomPick = (max) => Math.floor(Math.random() * max);
    };

  class Player1 extends Player {
    constructor(batu, kertas, gunting, choice) {
      super(batu, kertas, gunting, choice);
      this.batu[0].id = "batu-player";
      this.kertas[0].id = "kertas-player";
      this.gunting[0].id = "gunting-player";
    }
  }

  class Player2 extends Computer(Player) {
    constructor(batu, kertas, gunting, choice) {
      super(batu, kertas, gunting, choice);
      this.batu[1].id = "batu-com";
      this.kertas[1].id = "kertas-com";
      this.gunting[1].id = "gunting-com";
    }
  }

  class Rules {
    constructor() {
      this.resultText = document.createElement("H1");
      this.resultContainer = document.getElementById("vs_result");
      this.gamesResult = "Not decided yet!";
      this.round = 0;
    }

    logger = (text) => console.log("----------", text);

    _defaultState = () => {
      this.resultContainer.classList.remove("draw");
      this.resultContainer.classList.remove("versus_result");
      this.resultText.innerHTML = "VS";
      this.resultContainer.appendChild(this.resultText);
    };

    _playerOneWin = (player) => {
      this.resultContainer.classList.remove("draw");
      this.resultContainer.classList.add("versus_result");
      this.resultText.innerHTML = "PLAYER WIN";
      this.resultContainer.appendChild(this.resultText);
      this.gamesResult = `${player.name} win`;
      this.logger(
        `Result : ${player.name} win at round ${this.round}, great ! :)`
      );
      // setGameStats({ ...gameStats, round: gameStats.round.push("P1") });
    };

    _playerTwoWin = (player) => {
      this.resultContainer.classList.remove("draw");
      this.resultContainer.classList.add("versus_result");
      this.resultText.innerHTML = "COM WIN";
      this.resultContainer.appendChild(this.resultText);
      this.gamesResult = `${player.name} win`;
      this.logger(
        `Result : ${player.name} win at round ${this.round}, awesome ! :)`
      );
      // setGameStats({ ...gameStats, round: gameStats.round.push("P2") });
    };

    _drawResult = () => {
      this.resultContainer.classList.add("versus_result");
      this.resultContainer.classList.add("draw");
      this.resultText.innerHTML = "DRAW";
      this.resultContainer.appendChild(this.resultText);
      this.gamesResult = `Draw`;
      this.logger(`Result : Draw at round ${this.round}, GG !`);
      // setGameStats({ ...gameStats, round: gameStats.round.push("X") });
    };

    decision = (playerOne, playerTwo) => {
      const p1Batu = playerOne.choice === "batu";
      const p1Kertas = playerOne.choice === "kertas";
      const p1Gunting = playerOne.choice === "gunting";

      const p2Batu = playerTwo.choice === "batu";
      const p2Kertas = playerTwo.choice === "kertas";
      const p2Gunting = playerTwo.choice === "gunting";

      if (
        (p1Batu && p2Batu) ||
        (p1Kertas && p2Kertas) ||
        (p1Gunting && p2Gunting)
      ) {
        this._drawResult();
      } else if (
        (p1Batu && p2Gunting) ||
        (p1Kertas && p2Batu) ||
        (p1Gunting && p2Kertas)
      ) {
        this._playerOneWin(playerOne.data);
      } else if (
        (p1Batu && p2Kertas) ||
        (p1Kertas && p2Gunting) ||
        (p1Gunting && p2Batu)
      ) {
        this._playerTwoWin(playerTwo.data);
      }

      this.round === 6 ? this.sendData(this.gamesResult) : (this.round += 1);

      dispatch({
        type: actionTypes.GAME.ROUND_UPDATE,
      });
    };
  }

  class Game extends Rules {
    constructor(gamesResult, matchResult) {
      super(gamesResult, matchResult);
      this.resetResult = document.querySelector(".reset");
      this.p1_winner = 1;
      this.p2_winner = 0;
      this.initiation();
    }

    initiation() {
      this.p1 = new Player1();
      this.p2 = new Player2();
      this.p1.data = { name: "SAMI" };
      this.p2.data = { name: "COM" };

      this._defaultState();
      this.resetButton();
    }

    getUserPick = (choice) => {
      this.p1.choice = choice;
      this.logger(`Player choose: ${choice}`);
      return this.p1.choice;
    };

    getComPick = (choice) => {
      this.p2.choice = choice;
      this.logger(`Com choose: ${choice}`);
      return this.p2.choice;
    };

    setPlayerOneListener = () => {
      this.p1.batu[0].onclick = () => {
        this.getUserPick("batu");
        this.p1.batu[0].classList.add("active_choice");
        this.p1.kertas[0].classList.remove("active_choice");
        this.p1.gunting[0].classList.remove("active_choice");
        this.removePlayerListener();
        this.comDecideResult();
      };

      this.p1.kertas[0].onclick = () => {
        this.getUserPick("kertas");
        this.p1.batu[0].classList.remove("active_choice");
        this.p1.kertas[0].classList.add("active_choice");
        this.p1.gunting[0].classList.remove("active_choice");
        this.removePlayerListener();
        this.comDecideResult();
      };

      this.p1.gunting[0].onclick = () => {
        this.getUserPick("gunting");
        this.p1.batu[0].classList.remove("active_choice");
        this.p1.kertas[0].classList.remove("active_choice");
        this.p1.gunting[0].classList.add("active_choice");
        this.removePlayerListener();
        this.comDecideResult();
      };
    };

    setPlayerTwoListener(choice) {
      switch (choice) {
        case "batu":
          this.getComPick("batu");
          this.p2.batu[1].classList.add("active_choice");
          this.p2.kertas[1].classList.remove("active_choice");
          this.p2.gunting[1].classList.remove("active_choice");
          break;
        case "kertas":
          this.getComPick("kertas");
          this.p2.batu[1].classList.remove("active_choice");
          this.p2.kertas[1].classList.add("active_choice");
          this.p2.gunting[1].classList.remove("active_choice");
          break;
        case "gunting":
          this.getComPick("gunting");
          this.p2.batu[1].classList.remove("active_choice");
          this.p2.kertas[1].classList.remove("active_choice");
          this.p2.gunting[1].classList.add("active_choice");
          break;
        default:
          break;
      }
    }

    removePlayerListener = () => {
      document.getElementsByClassName("batu")[0].disabled = true;
      document.getElementsByClassName("kertas")[0].disabled = true;
      document.getElementsByClassName("gunting")[0].disabled = true;
    };

    comDecideResult() {
      switch (this.p2.randomPick(3)) {
        case 2:
          this.setPlayerTwoListener("batu");
          this.result();
          break;
        case 1:
          this.setPlayerTwoListener("kertas");
          this.result();
          break;
        case 0:
          this.setPlayerTwoListener("gunting");
          this.result();
          break;
        default:
          break;
      }
    }

    resetButton() {
      this.resetResult.onclick = () => {
        if (this.round !== 6) {
          this.logger("Game restarted !");
          this._defaultState();
          document.querySelectorAll(".choice").forEach((userButton) => {
            userButton.classList.remove("active_choice");
            userButton.disabled = false;
          });
        } else {
          alert("Room has been finished");
        }
      };
    }

    play() {
      if (this.round !== 6) {
        alert("Play for 6 rounds");
        this.logger("Lets play traditional games!");
        this.setPlayerOneListener();
      } else {
        this.resultContainer.innerText = "Finished";
        alert("This room has been finished");
        this.resetResult.setAttribute("disabled", "true");
      }
    }

    sendData(result) {
      const { name } = this.p1.data;
      const db = collection.doc(user.uid);

      const updateWin = () => {
        db.update({
          win: firestoreIncrement,
        });
      };

      const updateLose = () =>
        db.update({
          lose: firestoreIncrement,
        });

      result === `${name} win` ? updateWin() : updateLose();

      db.update({
        gameHistory: firestoreArrayUnion("rock-paper-scrissor"),
      });

      alert("Game Has Been Finished !");
    }

    result = () =>
      setTimeout(() => {
        if (this.p1 && this.p2) {
          this.decision(this.p1, this.p2);
        }

        this.p1.choice = null;
        this.p2.choice = null;
      }, 400);
  }

  const game = new Game();
  game.play();
}

export default PlayGames;
