const env = process.env.NODE_ENV;
const DOMAIN =
  env === "production"
    ? process.env.NEXT_PUBLIC_domain
    : process.env.NEXT_PUBLIC_dev_domain;

export default DOMAIN;
