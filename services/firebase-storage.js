import { firebaseOldApp } from "./firebase";

const db = firebaseOldApp.firestore();

export default db;
