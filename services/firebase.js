import { initializeApp } from "firebase/app";
import firebase from "firebase/compat/app";
import "firebase/compat/firestore";
import "firebase/compat/storage";

const firebaseConfig = {
  apiKey: process.env.NEXT_PUBLIC_apiKey,
  authDomain: process.env.NEXT_PUBLIC_authDomain,
  projectId: process.env.NEXT_PUBLIC_projectId,
  storageBucket: process.env.NEXT_PUBLIC_storageBucket,
  messagingSenderId: process.env.NEXT_PUBLIC_messagingSenderId,
  appId: process.env.NEXT_PUBLIC_appId,
  measurementId: process.env.NEXT_PUBLIC_measurementId,
  databaseURL: process.env.NEXT_PUBLIC_databaseURL,
};

const firebaseApp = initializeApp(firebaseConfig);
const firebaseOldApp = firebase.initializeApp(firebaseConfig);
const firebaseStorage = firebase.initializeApp(firebaseConfig);
const firebaseTimeStamp = firebase.firestore.FieldValue.serverTimestamp();
const firestoreIncrement = firebase.firestore.FieldValue.increment(1);
const firestoreArrayUnion = (arr) => firebase.firestore.FieldValue.arrayUnion(arr);

export { firebaseOldApp, firebaseTimeStamp, firestoreIncrement, firestoreArrayUnion, firebaseStorage };
export default firebaseApp;
