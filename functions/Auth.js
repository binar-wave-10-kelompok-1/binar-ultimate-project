import store from "../redux";
import {
  auth,
  createUserWithEmailAndPassword,
  sendPasswordResetEmail,
  signOut,
} from "../services/firebase-auth";
import db from "../services/firebase-storage";

const register_ = (name, email, password) =>
  createUserWithEmailAndPassword(auth, email, password)
    .then((userCredential) => {
      const { user } = userCredential;
      db.collection("users").doc(user.uid).set({
        uid: user.uid,
        name,
        avatar: null,
        authProvider: "local",
        email,
        win: 0,
        lose: 0,
        gameHistory: null,
        achievement: null,
        score: 0,
      });

      store.dispatch({ type: "SHOW_LOADER" });
    })
    .catch((e) => {
      store.dispatch({ type: "HIDE_LOADER" });
      console.log(e);
    });

const sendResetEmailPassword = async (email) => {
  try {
    await sendPasswordResetEmail(auth, email);
    alert("Password reset link sent !, check your email");
  } catch (err) {
    console.error(err);
    alert(err.message);
  }
};

const logout = () => {
  signOut(auth)
    .then(() => {
      alert("Sign Out Successful");
    })
    .catch((error) => {
      alert("There is a problem with your Sign Out", error);
    });

  sessionStorage.removeItem("user");
};

export { auth, db, register_, sendResetEmailPassword, logout };
