import db from "../services/firebase-storage";
import { firebaseTimeStamp, firestoreArrayUnion } from "../services/firebase";

const collection = db.collection("games-0");

const createGames = (user, player) =>
  collection.add({
    uid: user.uid,
    datecreated: firebaseTimeStamp,
    player_1: player.name,
    player_2: "com",
    winner: null,
    score: null,
  });

const updateGames = (user) =>
  collection.where("uid", "==", user.uid).update({
    winner: null,
    score: null,
  });

const createLastGames = (player) =>
  collection.doc("Last Game").set({
    datecreated: firebaseTimeStamp,
    player_1: player,
    player_2: "com",
    winner: null,
    score: null,
  });

const updateLastGames = (win, num) =>
  collection.doc("Last Game").update({
    winner: win,
    score: num,
  });

const createLeaderBoards = () =>
  collection.doc("Leaderboards").set({
    datecreated: firebaseTimeStamp,
    players: [],
    scores: [],
  });

const updateLeaderBoards = (player, score) =>
  collection.doc("Leaderboards").update({
    datecreated: firebaseTimeStamp,
    players: firestoreArrayUnion(player),
    scores: firestoreArrayUnion(score),
  });

export {
  createGames,
  updateGames,
  createLastGames,
  updateLastGames,
  createLeaderBoards,
  updateLeaderBoards,
};
