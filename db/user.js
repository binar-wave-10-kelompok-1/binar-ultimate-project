import db from "../services/firebase-storage";

const collection = db.collection("users");

// user is destructuring from userCredential
const createUser = (user, name, email) =>
  collection.doc(user.uid).set({
    uid: user.uid,
    name,
    avatar: null,
    authProvider: "local",
    email,
    win: 0,
    lose: 0,
    gameHistory: [],
    achievement: [],
    score: 0,
  });

export { collection, createUser };
