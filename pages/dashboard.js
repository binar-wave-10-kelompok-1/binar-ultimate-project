import Link from "next/link";
import router from "next/router";
import { useEffect, useState } from "react";
import { Breadcrumb, Nav, Navbar, Container } from "react-bootstrap";
import { connect } from "react-redux";
import LayoutMain from "../components/layout/main";
import { collection } from "../db/user";
import { logout } from "../functions/Auth";
import styles from "../styles/Home.module.css";

const Dashboard = ({ user }) => {
  const [userData, setUserData] = useState({
    name: "Player One",
    win: 0,
    lose: 0,
  });

  useEffect(() => {
    !user && router.push("/login");
  }, [router, user]);

  useEffect(() => {
    user &&
      collection
        .doc(user.uid)
        .get()
        .then((doc) => setUserData(doc.data()));
  }, []);

  return (
    <LayoutMain>
      <Navbar collapseOnSelect expand="md" bg="dark" variant="dark">
        <Container>
          <Link href="/">
            <a className={`${styles.nav_item} text-white px-2`}>
              Player Dashboard
            </a>
          </Link>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="me-auto">
              <Link href="/games/game-list">
                <a className={`${styles.nav_item} text-white me-2`}>
                  Game List
                </a>
              </Link>
              <Link href="/profile">
                <a className={`${styles.nav_item} text-white`}>Profile</a>
              </Link>
            </Nav>
            <Nav>
              <Nav.Link href="#" onClick={logout}>
                Logout
              </Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>

      <Container>
        <Breadcrumb className="bg-transparent my-3 fs-5">
          <Breadcrumb.Item href="/">Home</Breadcrumb.Item>
          <Breadcrumb.Item active>Dashboard</Breadcrumb.Item>
        </Breadcrumb>

        <main className="d-grid" style={{ minHeight: `${60}vh` }}>
          <h1 className="text-center m-auto font-weight-bold">
            Hi {userData.name}, welcome to your Dashboard
          </h1>
          <div>
            <p className="text-center">My total win : {userData.win}</p>
            <p className="text-center">My total lose: {userData.lose}</p>
          </div>
        </main>
      </Container>
    </LayoutMain>
  );
};

const mapStateToProps = (state) => state.user;

export default connect(mapStateToProps)(Dashboard);
