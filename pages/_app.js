import Script from "next/script";
import { useEffect } from "react";
import { Provider } from "react-redux";
import store from "../redux";
import FullpageLoader from "../components/FullpageLoader";
import "bootstrap/dist/css/bootstrap.min.css";
import "../styles/globals.css";
import { auth, onAuthStateChanged } from "../services/firebase-auth";
import setSession from "../hooks/setSession";
import actionTypes from "../redux/action";

function MyApp({ Component, pageProps }) {
  useEffect(
    () =>
      window
        ? onAuthStateChanged(auth, (authUser) => {
            setSession("user", JSON.stringify(authUser));
            store.dispatch({
              type: actionTypes.SET_USER,
              user: authUser,
            });
          })
        : null,
    []
  );
  return (
    <Provider store={store}>
      <Component {...pageProps} />

      <FullpageLoader />

      <Script
        type="module"
        src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"
        strategy="lazyOnload"
      />
      <Script
        nomodule
        src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"
        strategy="lazyOnload"
      />
    </Provider>
  );
}

export default MyApp;
