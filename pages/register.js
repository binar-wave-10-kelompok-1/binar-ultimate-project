import { useEffect, useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import { Form } from "react-bootstrap";
import { register_ } from "../functions/Auth";
import LoginGoogleButton from "../components/Button/LoginGoogleButton";
import styles from "../styles/Register.module.css";
import store from "../redux";
import LayoutMain from "../components/layout/main";

function Register() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [name, setName] = useState("");
  const router = useRouter();

  const register = (e) => {
    e.preventDefault();

    if (!name) {
      alert("Please enter name");
    } else if (!email) {
      alert("Please enter email");
    } else if (!password) {
      alert("Please enter password");
    } else {
      store.dispatch({ type: "SHOW_LOADER" });

      register_(name, email, password).then(() => router.push("/dashboard"));
    }
  };

  useEffect(() => {
    const { user } = store.getState().user;
    user && router.push("/dashboard");
  }, [router]);

  useEffect(() => {
    const subscribe = () => store.dispatch({ type: "HIDE_LOADER" });
    return () => subscribe();
  }, []);

  return (
    <LayoutMain>
      <div className={styles.register}>
        <div className={styles.register__container}>
          <Form>
            <Form.Group className="mb-3" controlId="formName">
              <Form.Control
                type="text"
                className={styles.register__textBox}
                value={name}
                onChange={(e) => setName(e.target.value)}
                placeholder="Full Name"
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formEmail">
              <Form.Control
                type="email"
                className={styles.register__textBox}
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                placeholder="E-mail Address"
                required
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formPassword">
              <Form.Control
                type="password"
                className={styles.register__textBox}
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                placeholder="Password"
                required
              />
              <Form.Text className="text-muted">
                We&apos;ll never share your password with anyone else.
              </Form.Text>
            </Form.Group>
            <Link href="/dashboard">
              <button
                type="button"
                className={styles.register__btn}
                onClick={register}
              >
                Register
              </button>
            </Link>
            <LoginGoogleButton text="Register with Google" />
            <div className={styles.register_login}>
              Already have an account? <Link href="/login">Login</Link> now.
            </div>
          </Form>
        </div>
      </div>
    </LayoutMain>
  );
}
export default Register;
