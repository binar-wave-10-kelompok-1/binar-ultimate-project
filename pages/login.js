import { useEffect, useState } from "react";
import { connect } from "react-redux";
import Link from "next/link";
import { Form } from "react-bootstrap";
import router from "next/router";
import LoginGoogleButton from "../components/Button/LoginGoogleButton";
import LoginButton from "../components/Button/LoginButton";
import styles from "../styles/Login.module.css";
import store from "../redux";
import LayoutMain from "../components/layout/main";

function Login({ user }) {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  useEffect(() => {
    user && router.push("/dashboard");
  }, [router]);

  useEffect(() => {
    const subscribe = () => store.dispatch({ type: "HIDE_LOADER" });
    return () => subscribe();
  }, []);

  return (
    <LayoutMain>
      <div className={styles.login}>
        <div className={styles.login__container}>
          <Form>
            <Form.Group className="mb-3" controlId="formEmail">
              <Form.Control
                type="email"
                className={styles.login__textBox}
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                placeholder="E-mail Address"
                aria-label="Email address"
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formPassword">
              <Form.Control
                type="password"
                className={styles.login__textBox}
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                placeholder="Password"
                aria-label="Password"
              />
            </Form.Group>

            <LoginButton email={email} password={password} />
            <LoginGoogleButton type="submit" text="Login with Google" />

            <div className={styles.login_register}>
              <Link href="/reset">Forgot Password</Link>
            </div>
            <div className={styles.login_register}>
              Don&apos;t have an account? <Link href="/register">Register</Link>{" "}
              now.
            </div>
          </Form>
        </div>
      </div>
    </LayoutMain>
  );
}

const mapStateToProps = (state) => state.user;

export default connect(mapStateToProps)(Login);
