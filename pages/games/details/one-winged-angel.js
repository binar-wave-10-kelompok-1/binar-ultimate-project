import { Breadcrumb } from "react-bootstrap";
import Link from "next/link";
import ReactPlayer from "react-player";
import styles from "../../../styles/GameDetails.module.css";
import "bootstrap/dist/css/bootstrap.min.css";

const onewing = () => (
  <div className={styles.gamedetails}>
    <div className={styles.gamedetails_container}>
      <h1 className={styles.game_details_title}>ONE WINGED ANGEL</h1>
      <Breadcrumb className="rounded bg-secondary my-3 px-2 fs-5">
        <Breadcrumb.Item>
          <Link href="/">Home</Link>
        </Breadcrumb.Item>
        <Breadcrumb.Item>
          <Link href="../../games/game-list">List</Link>
        </Breadcrumb.Item>
        <Breadcrumb.Item active className="text-dark">
          One Winged Angel
        </Breadcrumb.Item>
      </Breadcrumb>
      <br />
      <div className={`${styles.row} row`}>
        <div className={`${styles.col_8} col-8 ${styles.sephirot}`}>
          {/* <img className={styles.bkg2}  src="/images/game-list/gamemusic/one-winged-angel.jpg"
            alt="Sephirot" /> */}
          <ReactPlayer
            className={styles.bkg3}
            controls
            url="/music/one winged angel.mp3"
          />
        </div>
      </div>
      <div className={`${styles.row} row`}>
        <div className={`${styles.col_8} col-8`}>
          <p className={styles.gamedetails_text}>
            Game: Final Fantasy VII <br />
            Performance: The Black Mages <br />
            Composer: Nobuo Uematsu <br />
          </p>
        </div>
      </div>
    </div>
  </div>
);

export default onewing;
