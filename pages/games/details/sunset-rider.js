import { useState } from "react";
import { Breadcrumb } from "react-bootstrap";
import Link from "next/link";
import ReactPlayer from "react-player";
import styles from "../../../styles/GameDetails.module.css";
import "bootstrap/dist/css/bootstrap.min.css";
import getName from "../../../functions/RandomName";
import getScore from "../../../functions/RandomScore";



const axelay = () => {
  const [count, setCount] = useState(0);
  const [randomname, setName] = useState("-");
  const [randomscore, setScore] = useState(0);

  const onHandle = (e) => {
    e.preventDefault();
    setCount(count + 1);
    setName(getName());
    setScore(getScore());
  };

  return (
    <div className={styles.gamedetails}>
      <div className={styles.gamedetails_container}>
        <h1 className={styles.game_details_title}>SUNSET RIDER</h1>
        <Breadcrumb className="rounded bg-secondary my-3 px-2 fs-5">
          <Breadcrumb.Item>
            <Link href="/">Home</Link>
          </Breadcrumb.Item>
          <Breadcrumb.Item>
            <Link href="../../games/game-list">List</Link>
          </Breadcrumb.Item>
          <Breadcrumb.Item active className="text-dark">
            sunset rider
          </Breadcrumb.Item>
        </Breadcrumb>
        <br />
        <div className={`${styles.row} row`}>
          <div className={`${styles.col_8} col-8`}>
            <ReactPlayer 
              className={styles.bkg} 
              controls="true"
              playing="true"
              url="https://youtu.be/84Dlbv_nBo4"/>

          
          </div>
          <div className={`${styles.col_2} ${styles.leaderboard} col-2`}>
            TOP PLAYER LEADERBOARD
            <table className={styles.table_leaderboard}>
              <tbody>
                <tr className={styles.tr}>
                  <th className={styles.th}>
                    <p>Name</p>
                  </th>
                  <th className={styles.th}>
                    <p>Score</p>
                  </th>
                </tr>
                <tr className={styles.tr}>
                  <td className={styles.td}>{randomname}</td>
                  <td className={styles.td}>{randomscore}</td>
                </tr>
              </tbody>
            </table>
            Total Play: {count}
          </div>
        </div>
        <div className={`${styles.row} row`}>
          <div className={`${styles.col_8} col-8`}>
            <p className={styles.gamedetails_text}>
              Revolves around four bounty hunters named Steve, Billy Cool, Bob,
              and Cormano Wild, who are out to claim rewards offered for
              eliminating the most wanted outlaws in the West.
            </p>

            <button type="button" className={styles.button1} onClick={onHandle}>
              PLAY
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default axelay;
