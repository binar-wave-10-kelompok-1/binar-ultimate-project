import { Breadcrumb } from "react-bootstrap";
import Link from "next/link";
import styles from "../../../styles/GameDetails.module.css";
import "bootstrap/dist/css/bootstrap.min.css";

const BatuKertasGunting = () => (
  <div className={styles.gamedetails}>
    <div className={styles.gamedetails_container}>
      <h1 className={styles.game_details_title}>Batu Kertas Gunting</h1>
      <Breadcrumb className="rounded bg-secondary my-3 px-2 fs-5">
        <Breadcrumb.Item>
          <Link href="/">Home</Link>
        </Breadcrumb.Item>
        <Breadcrumb.Item>
          <Link href="../../games/game-list">List</Link>
        </Breadcrumb.Item>
        <Breadcrumb.Item active className="text-dark">
          batu-kertas-gunting
        </Breadcrumb.Item>
      </Breadcrumb>
      <br />
      <div className={`${styles.row} row`}>
        <div className={`${styles.col_8} col-8`}>
          <img
            className={styles.bkg}
            alt="games background"
            src="/images/game-details/the-game.jpg"
          />
        </div>
        <div className={`${styles.col_2} ${styles.leaderboard} col-2`}>
          TOP 5 Leaderboard
          <table className={styles.table_leaderboard}>
            <tbody>
              <tr className={styles.tr}>
                <th className={styles.th}>
                  <p>Name</p>
                </th>
                <th className={styles.th}>
                  <p>Score</p>
                </th>
              </tr>
              <tr className={styles.tr}>
                <td className={styles.td}>Rudi</td>
                <td className={styles.td}>30</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <div className={`${styles.row} row`}>
        <div className={`${styles.col_8} col-8`}>
          <p className={styles.gamedetails_text}>
            An Traditional Game player vs player
          </p>
          <Link href="/games/batu-kertas-gunting">
            <button type="button" className={styles.button1}>
              PLAY
            </button>
          </Link>
        </div>
      </div>
    </div>
  </div>
);

export default BatuKertasGunting;
