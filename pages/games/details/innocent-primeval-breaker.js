import { Breadcrumb } from "react-bootstrap";
import Link from "next/link";
import ReactPlayer from "react-player";
import styles from "../../../styles/GameDetails.module.css";
import "bootstrap/dist/css/bootstrap.min.css";

const innocent = () => (
  <div className={styles.gamedetails}>
    <div className={styles.gamedetails_container}>
      <h1 className={styles.game_details_title}>INNOCENT PRIMEVAL BREAKER</h1>
      <Breadcrumb className="rounded bg-secondary my-3 px-2 fs-5">
        <Breadcrumb.Item>
          <Link href="/">Home</Link>
        </Breadcrumb.Item>
        <Breadcrumb.Item>
          <Link href="../../games/game-list">List</Link>
        </Breadcrumb.Item>
        <Breadcrumb.Item active className="text-dark">
          Innocent Primeval Breaker
        </Breadcrumb.Item>
      </Breadcrumb>
      <br />
      <div className={`${styles.row} row`}>
        <div className={`${styles.col_8} col-8 `}>
          <ReactPlayer
            className={styles.bkg3}
            controls
            url="https://soundcloud.com/user-363225968/innocent-primeval-breaker"
          />
        </div>
      </div>
      <div className={`${styles.row} row`}>
        <div className={`${styles.col_8} col-8`}>
          <p className={styles.gamedetails_text}>
            Game: YS Seven <br />
            Performance: Falcom Sound team JDK <br />
            Composer: Falcom Sound team JDK <br />
          </p>
        </div>
      </div>
    </div>
  </div>
);

export default innocent;
