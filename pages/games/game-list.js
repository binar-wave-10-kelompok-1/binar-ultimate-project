import { Breadcrumb } from "react-bootstrap";
import Link from "next/link";
import styles from "../../styles/GameList.module.css";
import "bootstrap/dist/css/bootstrap.min.css";

const GameList = () => (
  <div className={styles.gamelist}>
    <div className={styles.container}>
      <h1 className="text-center text-light">List Game</h1>
      <Breadcrumb className="rounded bg-secondary my-3 px-2 fs-5">
        <Breadcrumb.Item>
          <Link href="/">Home</Link>
        </Breadcrumb.Item>
        <Breadcrumb.Item active className="text-light">
          List
        </Breadcrumb.Item>
      </Breadcrumb>
      <br />
      <div className="row p-4">
        <div className="col">
          <div data-testid="cardCheck" className={`${styles.card} card`}>
            <img
              className={styles.card_img_top}
              src="/images/game-list/gametitle/logo.png"
              alt="Card"
            />
            <div className="card-body">
              <h4 className={styles.card_title}>Batu Kertas Gunting</h4>
              <p className={styles.card_text}>Play Batu Kertas Gunting</p>
              <Link href="/games/details/batu-kertas-gunting">
                <button type="button" className={styles.button1}>
                  Details
                </button>
              </Link>
            </div>
          </div>
        </div>
        <div className="col">
          <div className={styles.card}>
            <img
              className={styles.card_img_top}
              src="/images/game-list/gametitle/axelay.jpg"
              alt="Card"
            />
            <div className="card-body">
              <h4 className={styles.card_title}>Axelay</h4>
              <p className={styles.card_text}>
                Play an classic shootem up game
              </p>
              <Link href="/games/details/axelay">
                <button type="button" className={styles.button1}>
                  Details
                </button>
              </Link>
            </div>
          </div>
        </div>
        <div className="col">
          <div className={styles.card}>
            <img
              className={styles.card_img_top}
              src="/images/game-list/gametitle/finalfight3.jpg"
              alt="Card"
            />
            <div className="card-body">
              <h4 className={styles.card_title}>Final Fight III</h4>
              <p className={styles.card_text}>
                Save Metro City from new Enemies
              </p>
              <Link href="/games/details/final-fight-3">
                <button type="button" className={styles.button1}>
                  Details
                </button>
              </Link>
            </div>
          </div>
        </div>
      </div>

      <div className="row p-4">
        <div className="col">
          <div className={styles.card}>
            <img
              className={styles.card_img_top}
              src="/images/game-list/gametitle/ninjagaidentrilogy.jpg"
              alt="Card"
            />
            <div className="card-body">
              <h4 className={styles.card_title}>Ninja Gaiden Trilogy</h4>
              <p className={styles.card_text}>Challenge Trilogy ninja game</p>
              <Link href="/games/details/ninja-gaiden-trilogy">
                <button type="button" className={styles.button1}>
                  Details
                </button>
              </Link>
            </div>
          </div>
        </div>
        <div className="col">
          <div className={styles.card}>
            <img
              className={styles.card_img_top}
              src="/images/game-list/gametitle/shinobi3.jpg"
              alt="Card"
            />
            <div className="card-body">
              <h4 className={styles.card_title}>Shinobi 3</h4>
              <p className={styles.card_text}>Classic adventure ninja game</p>
              <Link href="/games/details/shinobi-3">
                <button type="button" className={styles.button1}>
                  details
                </button>
              </Link>
            </div>
          </div>
        </div>
        <div className="col">
          <div className={styles.card}>
            <img
              className={styles.card_img_top}
              src="/images/game-list/gametitle/sunsetrider.jpg"
              alt="Card"
            />
            <div className="card-body">
              <h4 className={styles.card_title}>Sunset Rider</h4>
              <p className={styles.card_text}>Become cowboys in wild west</p>
              <Link href="/games/details/sunset-rider">
                <button type="button" className={styles.button1}>
                  Details
                </button>
              </Link>
            </div>
          </div>
        </div>
      </div>

      <div className="row p-4">
        <div className="col">
          <div className={styles.card}>
            <img
              className={styles.card_img_top}
              src="/images/game-list/gamemusic/one-winged-angel.jpg"
              alt="Card"
            />
            <div className="card-body">
              <h4 className={styles.card_title}>ONE WINGED ANGEL</h4>
              <p className={styles.card_text}>Ost Final Fantasy VII</p>
              <Link href="/games/details/one-winged-angel">
                <button type="button" className={styles.button1}>
                  Details
                </button>
              </Link>
            </div>
          </div>
        </div>
        <div className="col">
          <div className={styles.card}>
            <img
              className={styles.card_img_top}
              src="/images/game-list/gamemusic/innocent.jpg"
              alt="Card"
            />
            <div className="card-body">
              <h4 className={styles.card_title}>INNOCENT PRIMEVAL BREAKER</h4>
              <p className={styles.card_text}>Ost YS SEVEN</p>
              <Link href="/games/details/innocent-primeval-breaker">
                <button type="button" className={styles.button1}>
                  details
                </button>
              </Link>
            </div>
          </div>
        </div>
        <div className="col">
          <div className={styles.card}>
            <img
              className={styles.card_img_top}
              src="/images/game-list/gamemusic/wake-up.jpg"
              alt="Card"
            />
            <div className="card-body">
              <h4 className={styles.card_title}>Wake up, Get Up, Get out there</h4>
              <p className={styles.card_text}>ost Persona 5</p>
              <Link href="/games/details/wake-up">
                <button type="button" className={styles.button1}>
                  Details
                </button>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </div>
    ;
  </div>
);

export default GameList;
