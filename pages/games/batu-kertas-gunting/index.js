import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Link from "next/link";
import { useRouter } from "next/router";
import PlayGames from "../../../utils/RockPaperScrissor";
import ButtonChoice from "../../../components/ButtonChoice";
import styles from "../../../styles/Game_1.module.css";
import LayoutMain from "../../../components/layout/main";

const RockPaperScrissor = () => {
  const user = useSelector((state) => state.user.user);
  const games = useSelector((state) => state.games);

  const dispatch = useDispatch();
  const router = useRouter();

  const [player, setPlayer] = useState({
    displayName: "Player One",
    email: null,
  });

  useEffect(() => {
    if (!user) {
      router.push("/");
    } else {
      setPlayer({
        displayName: user.displayName,
        email: user.email,
      });

      PlayGames(user, games, dispatch);
    }
  }, []);

  useEffect(() => {
    console.log(games);
  }, [games]);

  return (
    <LayoutMain className={styles.gameBody}>
      <header>
        <nav className={styles.gameNavigation}>
          <div className={styles.back_button}>
            <Link
              href="/"
              className="d-flex"
              alt="go back"
              // eslint-disable-next-line react/style-prop-object
            >
              👈
            </Link>
          </div>
          <div className={styles.title}>
            <div className={styles.title_logo}>
              <img
                loading="lazy"
                width="80px"
                height="80px"
                src="/images/logo.png"
                alt="game's logo"
              />
            </div>
            <div className={styles.title_text}>
              <h1 id="game" className="text-uppercase">
                Games Room
              </h1>
            </div>
          </div>
        </nav>
      </header>

      <div className={styles.container}>
        <main className={styles.main}>
          <div className={styles.player}>
            <div className={styles.user_title}>
              <h2
                className="text-uppercase"
                id="player"
                data-id="{ player.id }"
                data-username="{ player.username }"
              >
                {player.displayName}
              </h2>
            </div>
            <div className={styles.user_choice}>
              <ButtonChoice choice="batu" />
              <ButtonChoice choice="kertas" />
              <ButtonChoice choice="gunting" />
            </div>
          </div>

          <div id="vs_result" className={styles.versus}>
            <div id="currentInfo" className={styles.info}>
              <h2>Game Info</h2>
              <h5>
                Round : {games?.currentRound !== 6 ? games?.currentRound : 6}
              </h5>
              <h5>Score : {games?.score}</h5>
            </div>
          </div>

          <div className={styles.player}>
            <div className={styles.user_title}>
              <h2 id="player-2" data-id="" data-username="COM">
                COM
              </h2>
            </div>
            <div className={styles.user_choice}>
              <ButtonChoice choice="batu" />
              <ButtonChoice choice="kertas" />
              <ButtonChoice choice="gunting" />
            </div>
          </div>

          <div className={styles.reset}>
            <button className="reset" type="button">
              <img
                loading="lazy"
                width="68px"
                height="68px"
                src="/images/refresh.png"
                alt="reset button"
              />
            </button>
          </div>
        </main>
      </div>
    </LayoutMain>
  );
};

export default RockPaperScrissor;
