import LayoutMain from "../components/layout/main";
import {
  Hero,
  Features,
  Testimonial,
  Navigation,
  Footer,
} from "../components/Home";
import styles from "../styles/Home.module.css";

export default function Home() {
  return (
    <LayoutMain className={styles.container}>
      <main className={styles.main}>
        <Navigation />
        <Hero />
        <Features />
        <Testimonial />
        <Footer />
      </main>
    </LayoutMain>
  );
}
