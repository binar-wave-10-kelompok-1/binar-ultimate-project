import { useEffect, useState } from "react";
import ProfileLayout from "../components/layout/profile";
import db from "../services/firebase-storage";
import styles from "../styles/Profile.module.css";

function AllUsers() {
  const [Users, setUsers] = useState([
    {
      uid: "",
      name: "",
      avatar: "",
      win: 0,
      score: 0,
      lose: 0,
    },
  ]);

  useEffect(() => {
    db.collection("users")
      .get()
      .then((querySnapshot) => {
        const documents = querySnapshot.docs.map((doc) => doc.data());
        setUsers(documents);
      });
  }, []);

  return (
    <ProfileLayout>
      <div className="px-3">
        <div className={styles.all_users}>
          {Users.map((user) => (
            <div className={styles.card_user} key={user.uid}>
              <div className="d-flex align-items-center">
                <div className={styles.avatar_user_wrap}>
                  {!user.avatar ? (
                    <img
                      src="/images/user-1.svg"
                      alt="avatar_user"
                      className={styles.avatar_user}
                    />
                  ) : (
                    <img
                      src={user.avatar}
                      alt="avatar_user"
                      className={styles.avatar_user}
                    />
                  )}
                </div>
                <p>{user.name}</p>
              </div>
              <div className="my-3">
                <p className="mb-2">win : {user.win}</p>
                <p className="mb-2">lose : {user.lose}</p>
                <p className="mb-2">score : {user.score}</p>
              </div>
            </div>
          ))}
        </div>
      </div>
    </ProfileLayout>
  );
}

export default AllUsers;
