import { useEffect, useState } from "react";
import Link from "next/link";
import store from "../redux";
import { sendResetEmailPassword } from "../functions/Auth";
import styles from "../styles/Reset.module.css";
import LayoutMain from "../components/layout/main";

function Reset() {
  const [email, setEmail] = useState("");

  useEffect(() => {
    const subscribe = () => store.dispatch({ type: "HIDE_LOADER" });
    return () => subscribe();
  }, []);

  return (
    <LayoutMain>
      <div className={styles.reset}>
        <div className={styles.reset__container}>
          <input
            type="text"
            className={styles.reset__textBox}
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            placeholder="E-mail Address"
          />
          <button
            type="button"
            className={styles.reset__btn}
            onClick={() => sendResetEmailPassword(email)}
          >
            Send password reset email
          </button>
          <div className={styles.reset_register}>
            Don&apos;t have an account? <Link href="/register">Register</Link>{" "}
            now.
          </div>
        </div>
      </div>
    </LayoutMain>
  );
}
export default Reset;

export async function getStaticProps() {
  return { props: { initialReduxState: store.getState() } };
}
