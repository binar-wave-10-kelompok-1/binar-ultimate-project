import { useEffect, useState } from "react";
import { connect } from "react-redux";
import db from "../services/firebase-storage";
import ProfileLayout from "../components/layout/profile";
import styles from "../styles/Profile.module.css";

function Profile(props) {
  const { user } = props;

  const [User, setUser] = useState({
    name: "",
    email: "",
    avatar: "",
  });
  const [image, setImage] = useState("");
  const [id, setId] = useState("");

  const uploadImage = async (e) => {
    const file = e.target.files[0];
    const data = new FormData();
    data.append("file", file);
    data.append("upload_preset", "efsxwwvd");

    const API = await fetch(
      "https://api.cloudinary.com/v1_1/dc3dree9l/image/upload",
      {
        method: "POST",
        body: data,
      }
    );

    const res = await API.json();
    setImage(res.secure_url);
  };

  const handleUpload = () => {
    const dataRef = db.collection("users").doc(id);
    return dataRef.update({
      avatar: image,
    });
  };

  useEffect(() => {
    db.collection("users").onSnapshot((querySnapshot) => {
      querySnapshot.forEach((doc) => {
        if (user.user.uid === doc.data().uid) {
          setUser(doc.data());
          setId(doc.data().uid);
        }
      });
    });
  }, []);

  return (
    <ProfileLayout>
      <div className="px-3">
        <div className={`${styles.profile_wrap} flex-column flex-md-row`}>
          <div className="px-3">
            <div className={styles.profile_picture}>
              {User.avatar ? (
                <img
                  src={User.avatar}
                  alt="avatar_user"
                  className={styles.profile_avatar}
                />
              ) : (
                <img
                  src="/images/user-1.svg"
                  alt="avatar_user"
                  className={styles.profile_avatar}
                />
              )}
              <div>
                <input
                  type="file"
                  className={styles.input_avatar}
                  onChange={uploadImage}
                />
                <button type="submit" className={styles.button_avatar}>
                  Pilih Foto
                </button>
                <button
                  type="submit"
                  onClick={handleUpload}
                  className={`${styles.button_avatar} my-3`}
                >
                  Submit Foto
                </button>
              </div>
            </div>
          </div>
          <div className={styles.data_profile_wrap}>
            <div className="data_profile">
              <div className="data_personal_wrap">
                <h3 className="mt-2 mb-4">Biodata Diri</h3>
                <div className={styles.data_personal}>
                  <p className={styles.data_personal_item}>Nama</p>
                  <p>{User.name}</p>
                </div>
                <div className={styles.data_personal}>
                  <p className={styles.data_personal_item}>Email</p>
                  <p>{User.email}</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </ProfileLayout>
  );
}

const mapStateToProps = (state) => ({
  user: state.user,
});

export default connect(mapStateToProps)(Profile);
