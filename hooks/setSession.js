const setSession = (...props) =>
  typeof window !== "undefined" ? sessionStorage.setItem(...props) : null;

export default setSession;
