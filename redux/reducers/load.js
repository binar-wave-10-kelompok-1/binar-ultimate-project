import actionTypes from "../action";

const initialState = {
  data: {},
  loading: false,
};

export default function loading(state = initialState, action) {
  switch (action.type) {
    case actionTypes.SHOW_LOADER:
      return { ...state, loading: true };

    case actionTypes.HIDE_LOADER:
      return { ...state, loading: false };

    default:
      return state;
  }
}
