import actionTypes from "../action";

const initialState = {
  currentRound: 0,
  round: [],
  roundStatus: [],
  score: 0,
};

export default function games(state = initialState, action) {
  const { GAME } = actionTypes;

  switch (action.type) {
    case GAME.ROUND_UPDATE:
      return {
        ...state,
        currentRound: state.currentRound + 1,
        round: state.round.concat(state.currentRound),
      };
    case GAME.STATUS_UPDATE:
      return { ...state, roundStatus: [...state.roundStatus, action.status] };
    case GAME.SCORE_UPDATE:
      return { ...state, score: action.score };
    default:
      return state;
  }
}
