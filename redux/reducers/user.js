import getSession from "../../hooks/getSession";
import actionTypes from "../action";

const initialState = { user: JSON.parse(getSession("user")) };

export default function user(state = initialState, action) {
  switch (action.type) {
    case actionTypes.SET_USER:
      return { ...state, user: action.user };
    default:
      return state;
  }
}
