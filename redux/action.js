const actionTypes = {
  SET_USER: "SET_USER",
  SHOW_LOADER: "SHOW_LOADER",
  HIDE_LOADER: "HIDE_LOADER",
  GAME: {
    ROUND_UPDATE: "ROUND_UPDATE",
    STATUS_UPDATE: "STATUS_UPDATE",
    SCORE_UPDATE: "SCORE_UPDATE",
  },
};

export default actionTypes;
