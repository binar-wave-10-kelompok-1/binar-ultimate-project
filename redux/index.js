import { createStore, combineReducers } from "redux";
import user from "./reducers/user";
import games from "./reducers/games";
import loading from "./reducers/load";

const reducer = combineReducers({
  user,
  games,
  loading,
});

const store = createStore(reducer);

export default store;
